cmake_minimum_required(VERSION 3.5.0)

set(Project AdapterNull)
project(${Project})

set(LIB_VERSION_MAJOR 1)
set(LIB_VERSION_MINOR 0)
set(LIB_VERSION_PATCH 0)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(PROJECT_SOURCE_DIR ./src)
set(PROJECT_HEADER_DIR ./include)

add_definitions(-D_CRT_SECURE_NO_WARNINGS)
add_definitions(-DWIN32_LEAN_AND_MEAN)

include_directories(${PROJECT_HEADER_DIR})
link_libraries(userenv ws2_32)    


set(SOURCE
${SOURCE}
${PROJECT_SOURCE_DIR}/AdapterNull.cpp
)

set(HEADERS
${HEADERS}
${PROJECT_HEADER_DIR}/UpdaterAdapter.h
${PROJECT_HEADER_DIR}/AdapterNull.h
)

add_library(${Project} SHARED ${SOURCE} ${HEADERS})

#set(CPACK_PACKAGE_FILE_NAME ${Project}.${CMAKE_SYSTEM_PROCESSOR}.${LIB_VERSION_MAJOR}.${LIB_VERSION_MINOR}.${LIB_VERSION_PATCH})
#set(CPACK_PACKAGE_DESCRIPTION, "Null Adapter Plugin")
#set(CPACK_GENERATOR "WIX")
#install(TARGETS ${Project} DESTINATION C:/usr/lib)
#install(FILES ${HEADERS} DESTINATION C:/usr/include/CoreLib)
#include(CPack)
