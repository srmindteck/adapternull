#include <stdio.h>
#include <stdlib.h>

#include "AdapterNull.h"

typedef struct adapter_t
{
    adapter_callback clback;
}adapter_t;

adapter_t* adapter = nullptr;

AdapterNotification notification;

const char* plug_in_identity = "NullAdapter";
AdapterKey* key = nullptr;

BOOL WINAPI DllMain(HMODULE hModule,
    DWORD  ul_reason_for_call,
    LPVOID lpReserved
)
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

LIBRARY_EXPORT const char* adapter_get_identity(adapter_t* plugin_object_ref)
{
    if (plugin_object_ref)
    {
        if (plugin_object_ref->clback != nullptr)
        {
            memset((void*)notification.Message, 0, 33);
            strcpy((char*)notification.Message, "get_identity");
            plugin_object_ref->clback(&notification);
        }

        return plug_in_identity;
    }

    return plug_in_identity;
}

LIBRARY_EXPORT const AdapterKey* adapter_get_pre_shared_key(adapter_t* plugin_object_ref)
{
    if (!key)
    {
        key = new AdapterKey();
        key->BufferSize = strlen(plug_in_identity) + 1;
        key->Buffer = new unsigned char[key->BufferSize];
        memset(key->Buffer, 0, key->BufferSize);
        memcpy(key->Buffer, plug_in_identity, strlen(plug_in_identity));
    }
    
    if (plugin_object_ref)
    {
        if (plugin_object_ref->clback != nullptr)
        {
            memset((void*)notification.Message, 0, 33);
            strcpy((char*)notification.Message, "get_pre_shared_key");
            plugin_object_ref->clback(&notification);
        }

        return key;
    }

    return key;
}

LIBRARY_EXPORT adapter_t* adapter_load(adapter_callback callback)
{
    adapter = new adapter_t();

    if (adapter)
    {
        adapter->clback = callback;

        notification.Message = new char[33];
        notification.MessageSize = 0;
        notification.Status = AdapterStatus::Updating;
        notification.Identity = new char[strlen(plug_in_identity) + 1];
        memset((void*)notification.Identity, 0, strlen(plug_in_identity) + 1);
        strcpy((char*)notification.Identity, plug_in_identity);

        if (adapter->clback != nullptr)
        {
            memset((void*)notification.Message, 0, 33);
            strcpy((char*)notification.Message, "load");
            adapter->clback(&notification);
        }
    }

    return adapter;
}

LIBRARY_EXPORT bool adapter_unload(adapter_t* plugin_object_ref)
{
    if (plugin_object_ref)
    {
        if (plugin_object_ref->clback != nullptr)
        {
            memset((void*)notification.Message, 0, 33);
            strcpy((char*)notification.Message, "unload");
            plugin_object_ref->clback(&notification);
        }

        delete notification.Message;
        notification.Message = nullptr;
        notification.MessageSize = 0;
        delete notification.Identity;
        delete key;
        delete plugin_object_ref;
        plugin_object_ref = nullptr;

        return true;
    }

    return false;
}

LIBRARY_EXPORT bool adapter_set_security(adapter_t* plugin_object_ref, const unsigned char* security_key, unsigned long key_len, const unsigned char* checksum_buffer, unsigned long checksum_len)
{
    if (plugin_object_ref)
    {
        if (plugin_object_ref->clback != nullptr)
        {
            memset((void*)notification.Message, 0, 33);
            strcpy((char*)notification.Message, "set_security");
            plugin_object_ref->clback(&notification);
        }

        return true;
    }

    return false;
}

LIBRARY_EXPORT bool adapter_software_update_load_from_buffer(adapter_t* plugin_object_ref, const unsigned char* buffer, unsigned long buffer_len)
{
    if (plugin_object_ref)
    {
        if (plugin_object_ref->clback != nullptr)
        {
            memset((void*)notification.Message, 0, 33);
            strcpy((char*)notification.Message, "software_update_load_from_buffer");
            plugin_object_ref->clback(&notification);
        }

        return true;
    }

    return false;
}

LIBRARY_EXPORT bool adapter_software_update_load_from_dirpath(adapter_t* plugin_object_ref, const unsigned char* dirpath, unsigned long dirpath_len)
{
    if (plugin_object_ref)
    {
        if (plugin_object_ref->clback != nullptr)
        {
            memset((void*)notification.Message, 0, 33);
            strcpy((char*)notification.Message, "software_update_load_from_dirpath");
            plugin_object_ref->clback(&notification);
        }

        return true;
    }

    return false;
}

LIBRARY_EXPORT bool adapter_software_update_apply(adapter_t* plugin_object_ref, unsigned long* version_no)
{
    if (plugin_object_ref)
    {
        if (plugin_object_ref->clback != nullptr)
        {
            memset((void*)notification.Message, 0, 33);
            strcpy((char*)notification.Message, "software_update_apply");
            plugin_object_ref->clback(&notification);
        }

        return true;
    }

    return false;
}

LIBRARY_EXPORT bool adapter_software_update_rollback(adapter_t* plugin_object_ref, const unsigned long version_no)
{
    if (plugin_object_ref)
    {
        if (plugin_object_ref->clback != nullptr)
        {
            memset((void*)notification.Message, 0, 33);
            strcpy((char*)notification.Message, "software_update_rollback");
            plugin_object_ref->clback(&notification);
        }

        return true;
    }

    return false;
}
