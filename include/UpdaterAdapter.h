#ifndef UPDATER_ADAPTER
#define UPDATER_ADAPTER

#include <stdint.h>
#include <stdbool.h>
#include <Windows.h>

#ifdef __cplusplus
extern "C" {
#endif

#define DECL_EXPORT __declspec(dllexport)
#define DECL_IMPORT __declspec(dllimport)

#if defined(LIBRARY_EXPORT)
#  define LIBRARY_EXPORT DECL_EXPORT
#else
#  define LIBRARY_EXPORT DECL_IMPORT
#endif

    /*
    * Opaque pointer to updater adapter, individual adapter's are free to implement as per their needs
    */
    typedef struct adapter_t adapter_t;

    /*
    * An update operation can have multiple states
    */
    typedef enum AdapterStatus
    {
        Updated = 'U',
        UpdateFailed = 'F',
        Updating = 'G',
        CannotUpdate = 'C',
        RolledBack = 'R'
    }AdapterStatus;

    /*
    * An update operation can have multiple states
    */
    typedef struct AdapterNotification
    {
        AdapterStatus Status; /* Status of the present update operation*/
        const char* Message; /* Message buffer*/
        unsigned long MessageSize; /* Size of the message buffer*/
        const char* Identity; /*NULL terminated C string, name or identity of the adapter*/
    }AdapterNotification;

    typedef struct AdapterKey
    {
        unsigned char* Buffer; /* Pre-shared key buffer*/
        unsigned long BufferSize; /* Size of the buffer*/
    }AdapterKey;

    /*
    * Pointer to callback function that shall be invoked by the adapter
    * The calling code must implement this function and pass on pointer during the adapter loading call
    */
    typedef void(*adapter_callback)(const AdapterNotification* notification);

    /*
    * DLL Entry point, all adapters must implement
    */
    BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpReserved);

    /*
    * Get the adapter identity, which can be a simple string
    * A valid pointer to the adapter object or NULL is provided
    * Return a const char* irrespective of what was provided in the calling arguement
    */
    extern LIBRARY_EXPORT const char* adapter_get_identity(adapter_t* adapter_object_ref);

    /*
    * Get the adapter's pre-shared key, which can be an encoded buffer
    * A valid pointer to the adapter object or NULL is provided
    * Return a const unsigned char* irrespective of what was provided in the calling arguement
    */
    extern LIBRARY_EXPORT const AdapterKey* adapter_get_pre_shared_key(adapter_t* adapter_object_ref);


    /*
    * Load the adapter, all initialization is performed here, a pointer to the callback is provided
    * A valid pointer to the adapter object is returned
    * Return NULL if adapter initialization fails
    */
    extern LIBRARY_EXPORT adapter_t* adapter_load(adapter_callback callback);

    /*
    * Unload the adapter, all deinitialization is performed here
    * A valid pointer to the adapter object is provided
    * Return C boolean, false if adapter deinitialization fails or else true
    */
    extern LIBRARY_EXPORT bool adapter_unload(adapter_t* adapter_object_ref);

    /*
    * Security setting is optional
    * A shared key that can check for signed hex buffers
    * A cheksum buffer that can be looked at the end of the hex buffer
    */
    extern LIBRARY_EXPORT bool adapter_set_security(adapter_t* adapter_object_ref, const unsigned char* security_key, unsigned long key_len, const unsigned char* checksum_buffer, unsigned long checksum_len);

    /*
    * Send the software update package to the adapter
    */
    extern LIBRARY_EXPORT bool adapter_software_update_load_from_buffer(adapter_t* adapter_object_ref, const unsigned char* buffer, unsigned long buffer_len);

    /*
    * Send the fully qualified path to file that contains the software package
    */
    extern LIBRARY_EXPORT bool adapter_software_update_load_from_dirpath(adapter_t* adapter_object_ref, const unsigned char* dirpath, unsigned long dirpath_len);

    /*
    * Apply/flash the software update to the target device
    * The adapter may optionally maintain a version number for this softwrae update
    * The version no should contain the new version number when the function returns.
    * It may be returned with 0 if version tracking is not performed
    */
    extern LIBRARY_EXPORT bool adapter_software_update_apply(adapter_t* adapter_object_ref, unsigned long* version_no);

    /*
    * Rollback the software update and re-apply the previous software update
    * The adapter should rollback to the previous version or to a specifc older version if the version number is provided.
    * Version number may be 0 if not used
    */
    extern LIBRARY_EXPORT bool adapter_software_update_rollback(adapter_t* adapter_object_ref, const unsigned long version_no);

#ifdef __cplusplus
}
#endif

#endif